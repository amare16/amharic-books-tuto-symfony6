<?php

namespace App\Controller;

use App\Repository\BookRepository;
use App\Repository\UploadPdfFilesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UploadPdfFilesController extends AbstractController
{
    /**
     * @var UploadPdfFilesRepository
     */
    private UploadPdfFilesRepository $uploadPdfFilesRepository;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var BookRepository
     */
    private BookRepository $bookRepository;

    public function __construct(UploadPdfFilesRepository $uploadPdfFilesRepository, BookRepository $bookRepository, EntityManagerInterface $em)
    {

        $this->uploadPdfFilesRepository = $uploadPdfFilesRepository;
        $this->em = $em;
        $this->bookRepository = $bookRepository;
    }

    #[Route('/upload/pdf/files', name: 'app_upload_pdf_files')]
    public function index(): Response
    {
        return $this->render('upload_pdf_files/index.html.twig', [
            'controller_name' => 'UploadPdfFilesController',
        ]);
    }

    #[Route('/books/delete-pdf/{id}', name: 'uploadPdfFile.delete', methods: ['GET', 'DELETE']), IsGranted('ROLE_ADMIN')]
    public function deleteUploadedPdfFile($id, Request $request) : Response
    {
        $uploadPdfFile = $this->uploadPdfFilesRepository->find($id);
        $pdfName = $this->getParameter('uploads_pdf_directory'). '/' . $uploadPdfFile->getName();

        if (file_exists($pdfName)) {
            unlink($pdfName);
        }
        $this->em->remove($uploadPdfFile);
        $this->em->flush();

        $route = $request->headers->get('referer');

        return $this->redirect($route);
    }
}
